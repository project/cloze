<?php


/**
 * The main classes for the short answer question type.
 *
 * These inherit or implement code found in quiz_question.classes.inc.
 *
 * If you are developing your own question type, the easiest place to start is with
 * multichoice.classes.inc. short_answer and long_answer are good for understanding
 * question types that involve textual answers.
 *
 * @file
 */

/**
 * Extension of QuizQuestion.
 *
 * This could have extended long answer, except that that would have entailed
 * adding long answer as a dependency.
 */
class ClozeQuestion extends QuizQuestion {

  /**
   * Implementation of saveNodeProperties
   *
   * @see QuizQuestion#saveNodeProperties($is_new)
   */
  public function saveNodeProperties($is_new = FALSE) {
    // no special property for cloze question node
  }

  /**
   * Implementation of validateNode
   *
   * @see QuizQuestion#validateNode($form)
   */
  public function validateNode(array &$form) {
    if (substr_count($this->node->body, '[') !== substr_count($this->node->body, ']')) {
      form_set_error('body', t('Please check the question format.'));
    }
  }

  /**
   * Implementation of delete
   *
   * @see QuizQuestion#delete($only_this_version)
   */
  public function delete($only_this_version = FALSE) {
    parent::delete($only_this_version);
    if ($only_this_version) {
      db_query('DELETE FROM {quiz_cloze_user_answers} WHERE question_nid = %d AND question_vid = %d', $this->node->nid, $this->node->vid);
    }
    else {
      db_query('DELETE FROM {quiz_cloze_user_answers} WHERE question_nid = %d', $this->node->nid);
    }
  }

  /**
   * Implementation of getNodeProperties
   *
   * @see QuizQuestion#getNodeProperties()
   */
  public function getNodeProperties() {
    if (isset($this->nodeProperties)) {
      return $this->nodeProperties;
    }
    $props = parent::getNodeProperties();
    $this->nodeProperties = $props;
    return $props;
  }

  /**
   * Implementation of getNodeView
   *
   * @see QuizQuestion#getNodeView()
   */
  public function getNodeView() {
    $content = parent::getNodeView();
    drupal_add_css(drupal_get_path('module', 'cloze') . '/theme/cloze.css');
    $chunks = _cloze_get_question_chunks($this->node->body);
    if ($this->viewCanRevealCorrect() && !empty($chunks)) {
      $solution = $this->node->body;
      foreach ($chunks as $position => $chunk) {
        if (strpos($chunk, '[') === FALSE) {
          continue;
        }
        $chunk = str_replace(array('[', ']'), '', $chunk);
        $choices = explode(',', $chunk);
        $replace = '<span class="correct answer user-answer">' . $choices[0] . '</span>';
        $solution = str_replace($chunks[$position], $replace, $solution);
      }
      $content['answers'] = array(
        '#type' => 'markup',
        '#value' => '<div class="quiz-solution cloze-question">'. check_markup($solution, variable_get('filter_default_format', 1)) .'</div>',
      );
    }
    else {
      $content['answers'] = array(
        '#type' => 'markup',
        '#value' => '<div class="quiz-answer-hidden">Answer hidden</div>',
        '#weight' => 2,
      );
    }
    return $content;
  }

  /**
   * Implementation of getAnsweringForm
   *
   * @see QuizQuestion#getAnsweringForm($form_state, $rid)
   */
  public function getAnsweringForm(array $form_state = NULL, $rid) {
    $form = parent::getAnsweringForm($form_state, $rid);
    $form['#theme'] = 'cloze_answering_form';
    drupal_add_css(drupal_get_path('module', 'cloze') . '/theme/cloze.css');
    $form['open_wrapper'] = array(
      '#type' => 'markup',
      '#value' => '<div class="cloze-question">',
    );
    foreach (_cloze_get_question_chunks($this->node->body) as $position => $chunk) {
      if (strpos($chunk, '[') === FALSE) {
        // this "tries[foobar]" hack is needed becaues question handler engine checks for input field
        // with name tries
        $form['tries['. $position .']'] = array(
          '#type' => 'markup',
          '#value' => str_replace("\n", "<br/>", $chunk),
          '#prefix' => '<div class="form-item">',
          '#suffix' => '</div>',
        );
      }
      else {
        $chunk = str_replace(array('[', ']'), '', $chunk);
        $choices = explode(',', $chunk);
        if (count($choices) > 1) {
          $form['tries['. $position .']'] = array(
            '#type' => 'select',
            '#title' => '',
            '#options' => _cloze_shuffle_choices(drupal_map_assoc($choices)),
            '#required' => FALSE,
          );
        }
        else {
          $form['tries['. $position .']'] = array(
            '#type' => 'textfield',
            '#title' => '',
            '#size' => 32,
            '#required' => FALSE,
            '#attributes' => array(
              'autocomplete' => 'off',
            ),
          );
        }
      }
    }
    $form['close_wrapper'] = array(
      '#type' => 'markup',
      '#value' => '</div>',
    );
    if (isset($rid)) {
      $cloze_esponse = new ClozeResponse($rid, $this->node);
      $response = $cloze_esponse->getResponse();
      if (is_array($response)) {
        foreach ($response as $key => $value) {
          $form["tries[$key]"]['#default_value'] = $value;
        }
      }
    }
    return $form;
  }

  /**
   * Implementation of getCreationForm
   *
   * @see QuizQuestion#getCreationForm($form_state)
   */
  public function getCreationForm(array $form_state = NULL) {
    drupal_add_css(drupal_get_path('module', 'cloze') . '/theme/cloze.css');
    $form['instructions'] = array(
      '#type' => 'markup',
      '#value' => '<div class="cloze-instruction">' . t('For free text cloze, mention the correct ansewr inside the square bracket. For multichoice cloze, provide the options separated by commas with correct answer as first. <br/>Example question: [The] Sun raises in the [east, west, north, south]. <br/>Answer: <span class="answer correct correct-answer">The</span> sun raises the <span class="answer correct correct-answer">east</span>.') . '</div>',
      '#weight' => -10,
    );
    return $form;

  }

  /**
   * Implementation of getMaximumScore
   *
   * @see QuizQuestion#getMaximumScore()
   */
  public function getMaximumScore() {
    return 10;
  }

  /**
   * Evaluate the correctness of an answer based on the correct answer and evaluation method.
   */
  public function evaluateAnswer($user_answer) {
    $correct_answer = _cloze_get_correct_answer_chunks($this->node->body);
    $total_answer = count($correct_answer);
    $correct_answer_count = 0;
    if ($total_answer == 0) {
      return $this->getMaximumScore();
    }
    foreach ($correct_answer as $key => $value) {
      if (drupal_strtolower($correct_answer[$key]) == drupal_strtolower($user_answer[$key])) {
        $correct_answer_count++;
      }
    }
    $score = $correct_answer_count / $total_answer * $this->getMaximumScore();
    return round($score);
  }
}

/**
 * Extension of QuizQuestionResponse
 */
class ClozeResponse extends QuizQuestionResponse {

  /**
   * ID of the answer.
   */
  protected $answer_id = 0;

  /**
   * Constructor
   */
  public function __construct($result_id, stdClass $question_node, $answer = NULL) {
    parent::__construct($result_id, $question_node, $answer);
    if (!isset($answer)) {
      $r = db_fetch_object(db_query('SELECT answer_id, answer, score, question_vid, question_nid, result_id FROM {quiz_cloze_user_answers} WHERE question_nid = %d AND question_vid = %d AND result_id = %d', $question_node->nid, $question_node->vid, $result_id));
      if (!empty($r)) {
        $this->answer = unserialize($r->answer);
        $this->score = $r->score;
        $this->answer_id = $r->answer_id;
        $this->answer_feedback = $r->answer_feedback;
      }
    }
    else {
      $this->answer = $answer;
    }
  }

  /**
   * Implementation of isValid
   *
   * @see QuizQuestionResponse#isValid()
   */
  public function isValid() {
    /*
    if (trim($this->answer) == '') {
      return t('You must provide an answer');
    }
    */
    return TRUE;
  }

  /**
   * Implementation of save
   *
   * @see QuizQuestionResponse#save()
   */
  public function save() {
    $sql = 'INSERT INTO {quiz_cloze_user_answers} (answer, question_nid, question_vid, result_id, score) VALUES ("%s", %d, %d, %d, %d)';
    db_query($sql, serialize($this->answer), $this->question->nid, $this->question->vid, $this->rid, $this->getScore(FALSE));
    $this->answer_id = db_last_insert_id('quiz_cloze_user_answers', 'answer_id');
  }

  /**
   * Implementation of delete
   *
   * @see QuizQuestionResponse#delete()
   */
  public function delete() {
    db_query('DELETE FROM {quiz_cloze_user_answers} WHERE question_nid = %d AND question_vid = %d AND result_id = %d', $this->question->nid, $this->question->vid, $this->rid);
  }

  /**
   * Implementation of score
   *
   * @see QuizQuestionResponse#score()
   */
  public function score() {
    $shortAnswer = new ClozeQuestion($this->question);
    $score = $shortAnswer->evaluateAnswer($this->answer);
    return $score;
  }

  /**
   * Implementation of getResponse
   *
   * @see QuizQuestionResponse#getResponse()
   */
  public function getResponse() {
    return $this->answer;
  }

  /**
   * Implementation of getReportFormResponse
   *
   * @see QuizQuestionResponse#getReportFormResponse($showpoints, $showfeedback, $allow_scoring)
   */
  public function getReportFormResponse($showpoints = TRUE, $showfeedback = TRUE, $allow_scoring = FALSE) {
    $form = array();
    $form['#theme'] = 'cloze_response_form';
    if ($this->question && !empty($this->question->answers)) {
      $answer = (object) current($this->question->answers);
    }
    else {
      return $form;
    }
    $question = $this->question->body;
    $correct_answer = _cloze_get_correct_answer($question);
    $user_answer = _cloze_get_user_answer($question, $this->answer);
    $form['answer'] = array(
      '#type' => 'markup',
      '#value' => theme('cloze_user_answer', $user_answer, $correct_answer),
    );
    return $form;
  }

  /**
   * Implementation of getReportFormScore
   *
   * @see QuizQuestionResponse#getReportFormScore($showpoints, $showfeedback, $allow_scoring)
   */
  public function getReportFormScore($showfeedback = TRUE, $showpoints = TRUE, $allow_scoring = FALSE) {
    $score = ($this->isEvaluated()) ? $this->getScore() : '?';
    if (quiz_access_to_score() && $allow_scoring && $this->question->correct_answer_evaluation == ShortAnswerQuestion::ANSWER_MANUAL) {
      return array(
        '#type' => 'textfield',
        '#default_value' => $score,
        '#size' => 3,
        '#maxlength' => 3,
        '#attributes' => array('class' => 'quiz-report-score'),
      );
    }
    else {
      return array(
        '#type' => 'markup',
        '#value' => $score,
      );
    }
  }
}
